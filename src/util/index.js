/* eslint no-console: 0 */
import HappyPack from 'happypack';
import notifier from 'node-notifier';
import appRootDir from 'app-root-dir';
import prettyjson from 'prettyjson';
import colors from 'colors/safe';
import { execSync } from 'child_process';

export function happyPackPlugin({ name, loaders }) {
  return new HappyPack({
    id: name,
    verbose: false,
    threads: 5,
    loaders,
  });
}

export function removeEmpty(x) {
  return x.filter(y => y != null);
}

export function ifElse(condition) {
  // TODO: Allow the then/or to accept a function for lazy value resolving.
  return function ifElseResolver(then, or) {
    const execIfFuc = x => (typeof x === 'function' ? x() : x);
    return condition ? execIfFuc(then) : (or);
  };
}

export function merge(...args) {
  const filtered = removeEmpty(args);
  if (filtered.length < 1) {
    return {};
  }
  if (filtered.length === 1) {
    return args[0];
  }
  return filtered.reduce((acc, cur) => {
    Object.keys(cur).forEach((key) => {
      if (typeof acc[key] === 'object' && typeof cur[key] === 'object') {
        // eslint-disable-next-line no-param-reassign
        acc[key] = merge(acc[key], cur[key]);
      } else {
        // eslint-disable-next-line no-param-reassign
        acc[key] = cur[key];
      }
    });
    return acc;
  }, {});
}

export function log(options) {
  const title = `${options.title.toUpperCase()}`;

  if (options.notify) {
    notifier.notify({
      title,
      message: options.message,
    });
  }

  const level = options.level || 'info';
  const msg = `==> ${title} -> ${options.message}`;

  switch (level) {
    case 'warn': console.log(colors.yellow(msg)); break;
    case 'error': console.log(colors.bgRed.white(msg)); break;
    case 'info':
    default: console.log(colors.green(msg));
  }
}

export function logJSON(data) {
  console.log(prettyjson.render(data));
}

export function exec(command) {
  execSync(command, { stdio: 'inherit', cwd: appRootDir.get() });
}
