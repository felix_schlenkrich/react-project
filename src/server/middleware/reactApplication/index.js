
import React from 'react';
import { renderToString } from 'react-dom/server';
import { ServerRouter, createServerRenderContext } from 'react-router';
import { CodeSplitProvider, createRenderContext } from 'code-split-component';
import Helmet from 'react-helmet';
import generateHTML from './generateHTML';
import App from '../../../shared/components/App';
import config from '../../../../config';

function reactApplicationMiddleware(request, response) {
  if (typeof response.locals.nonce !== 'string') {
    throw new Error('A "nonce" value has not been attached to the response');
  }
  const nonce = response.locals.nonce;

  if (config.disableSSR) {
    if (process.env.NODE_ENV === 'development') {
      // eslint-disable-next-line no-console
      console.log('==> Handling react route without SSR');
    }

    const html = generateHTML({
      nonce,
    });
    response.status(200).send(html);
    return;
  }

  const reactRouterContext = createServerRenderContext();

  const codeSplitContext = createRenderContext();

  const reactAppString = renderToString(
    <CodeSplitProvider context={codeSplitContext}>
      <ServerRouter location={request.url} context={reactRouterContext}>
        <App />
      </ServerRouter>
    </CodeSplitProvider>,
  );

  const html = generateHTML({
    reactAppString,
    nonce,
    // @see https://github.com/nfl/react-helmet
    helmet: Helmet.rewind(),
    codeSplitState: codeSplitContext.getState(),
  });

  const renderResult = reactRouterContext.getResult();

  if (renderResult.redirect) {
    response.status(301).setHeader('Location', renderResult.redirect.pathname);
    response.end();
    return;
  }

  response
    .status(
      renderResult.missed
        ? 404
        : 200,
    )
    .send(html);
}

export default (reactApplicationMiddleware);
