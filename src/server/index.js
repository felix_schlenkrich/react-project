
/* eslint no-console: 0 */
import express from 'express';
import appRootDir from 'app-root-dir';
import { resolve as pathResolve } from 'path';
import compression from 'compression';
import config from '../../config';
import security from './middleware/security';
import clientBundle from './middleware/clientBundle';
import reactApplication from './middleware/reactApplication';
import errorHandlers from './middleware/errorHandlers';


const app = express();

app.disable('x-powered-by');

app.use(...security);

// @see https://github.com/expressjs/compression
app.use(compression());

app.use(config.bundles.client.webPath, clientBundle);

app.use(express.static(pathResolve(appRootDir.get(), config.publicAssetsPath)));

app.get('*', reactApplication);

app.use(...errorHandlers);

const listener = app.listen(config.port, config.host, () =>
  console.log(`Server listening on port ${config.port}`),
);

export default listener;
