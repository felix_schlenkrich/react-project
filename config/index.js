import { getStringEnvVar, getIntEnvVar } from './internals/environmentVars';
import filterObject from './internals/filterObject';

if (process.env.IS_CLIENT) {
  throw new Error("You shouldn't be importing the `./config` directly into your 'client' or 'shared' source as the configuration object will get included in your client bundle. Not a safe move! Instead, use the `safeConfigGet` helper function (located at `./src/shared/utils/config`) within the 'client' or 'shared' source files to reference configuration values in a safe manner.");
}

const config = {

  host: getStringEnvVar('SERVER_HOST', 'localhost'),
  port: getIntEnvVar('SERVER_PORT', 3000),
  clientDevServerPort: getIntEnvVar('CLIENT_DEVSERVER_PORT', 7331),

  welcomeMessage: getStringEnvVar('WELCOME_MSG', 'Whoa!'),

  disableSSR: false,

  browserCacheMaxAge: '365d',

  publicAssetsPath: './public',
  buildOutputPath: './build',
  optimizeProductionBuilds: true,
  includeSourceMapsForProductionBuilds: false,
  bundlesSharedSrcPath: './src/shared',
  bundleSrcTypes: ['js', 'jsx', 'json'],
  bundleAssetTypes: [
    'jpg',
    'jpeg',
    'png',
    'gif',
    'ico',
    'eot',
    'svg',
    'ttf',
    'woff',
    'woff2',
    'otf',
  ],
  bundleAssetsFileName: 'assets.json',

  cspExtensions: {
    childSrc: [],
    connectSrc: [],
    defaultSrc: [],
    fontSrc: [],
    imgSrc: [],
    mediaSrc: [],
    manifestSrc: [],
    objectSrc: [],
    scriptSrc: [],
    styleSrc: [],
  },

  nodeBundlesIncludeNodeModuleFileTypes: [
    /\.(eot|woff|woff2|ttf|otf)$/,
    /\.(svg|png|jpg|jpeg|gif|ico)$/,
    /\.(mp4|mp3|ogg|swf|webp)$/,
    /\.(css|scss|sass|sss|less)$/,
  ],

  polyfillIO: {
    enabled: true,
    url: 'https://cdn.polyfill.io/v2/polyfill.min.js',
  },

  htmlPage: {
    htmlAttributes: { lang: 'en' },
    titleTemplate: 'React, Universally - %s',
    defaultTitle: 'React, Universally',
    meta: [
      {
        name: 'description',
        content: 'A starter kit giving you the minimum requirements for a production ready universal react application.',
      },
      { name: 'charset', content: 'utf-8' },
      { 'http-equiv': 'X-UA-Compatible', content: 'IE=edge' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'theme-color', content: '#2b2b2b' },
    ],
    links: [
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png', href: '/favicon-32x32.png', sizes: '32x32' },
      { rel: 'icon', type: 'image/png', href: '/favicon-16x16.png', sizes: '16x16' },
      { rel: 'mask-icon', href: '/safari-pinned-tab.svg', color: '#00a9d9' },
    ],
    scripts: [
      // Example:
      // { src: 'http://include.com/pathtojs.js', type: 'text/javascript' },
    ],
  },

  bundles: {
    client: {
      srcEntryFile: './src/client/index.js',
      srcPaths: [
        './src/client',
        './src/shared',
        './config',
      ],
      outputPath: './build/client',
      webPath: '/client/',

      devVendorDLL: {
        enabled: true,
        include: [
          'code-split-component',
          'react',
          'react-dom',
          'react-helmet',
          'react-router',
        ],

        name: '__dev_vendor_dll__',
      },
    },

    server: {
      srcEntryFile: './src/server/index.js',
      srcPaths: [
        './src/server',
        './src/shared',
        './config',
      ],
      outputPath: './build/server',
    },
  },

  additionalNodeBundles: {
    // NOTE: The webpack configuration and build scripts have been built so
    // that you can add arbitrary additional node bundle configurations here.
    //
    // A common requirement for larger projects is to add additional "node"
    // target bundles (e.g an APi server endpoint). Therefore flexibility has been
    // baked into our webpack config factory to allow for this.
    //
    // Simply define additional configurations similar to below.  The development
    // server will manage starting them up for you.  The only requirement is that
    // within the entry for each bundle you create and return the "express"
    // listener.
    /*
    apiServer: {
      srcEntryFile: './src/api/index.js',
      srcPaths: [
        './src/api',
        './src/shared',
        './config',
      ],
      outputPath: './build/api',
    }
    */
  },

  plugins: {
    babelConfig: (babelConfig, buildOptions) => {
      const { target, mode } = buildOptions;
      return babelConfig;
    },

    webpackConfig: (webpackConfig, buildOptions) => {
      const { target, mode } = buildOptions;
      return webpackConfig;
    },
  },
};

// Export the client configuration object.
export const clientConfig = filterObject(
  config,
  {
    welcomeMessage: true,
    polyfillIO: true,
    htmlPage: true,
    additionalNodeBundles: true,
  },
);

export default config;
