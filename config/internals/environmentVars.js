import dotenv from 'dotenv';
import fs from 'fs';
import path from 'path';
import appRootDir from 'app-root-dir';
import userHome from 'user-home';
import colors from 'colors/safe';
import pkg from '../../package.json';

function registerEnvFile() {
  const envName = process.env.NODE_ENV || 'development';
  const envFile = '.env';

  // Resolve an environment configuration
  const envFileResolutionOrder = [
    path.resolve(appRootDir.get(), `${envFile}.${envName}`),
    path.resolve(appRootDir.get(), envFile),
    path.resolve(userHome, '.config', pkg.name, `${envFile}.${envName}`),
    path.resolve(userHome, '.config', pkg.name, envFile),
  ];

  const envFilePath = envFileResolutionOrder.find(filePath => fs.existsSync(filePath));

  if (envFilePath) {
    console.log( // eslint-disable-line no-console
      colors.bgBlue.white(`==> Registering environment variables from: ${envFilePath}`),
    );
    dotenv.config({ path: envFilePath });
  }
}

registerEnvFile();

export function getStringEnvVar(name, defaultVal) {
  return process.env[name] || defaultVal;
}

export function getIntEnvVar(name, defaultVal) {
  return process.env[name]
    ? parseInt(process.env[name], 10)
    : defaultVal;
}

export function getBoolVar(name, defaultVal) {
  return process.env[name]
    ? process.env[name] === 'true'
    : defaultVal;
}
