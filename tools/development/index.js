import chokidar from 'chokidar';
import { resolve as pathResolve } from 'path';
import appRootDir from 'app-root-dir';
import { log } from '../../src/util';

let HotDevelopment = require('./hotDevelopment').default;

let devServer = new HotDevelopment();

const watcher = chokidar.watch([
  pathResolve(appRootDir.get(), 'tools'),
  pathResolve(appRootDir.get(), 'config'),
]);
watcher.on('ready', () => {
  watcher.on('change', () => {
    log({
      title: 'webpack',
      level: 'warn',
      message: 'Project build configuration has changed. Restarting the development devServer...',
    });
    devServer.dispose().then(() => {
      Object.keys(require.cache).forEach((modulePath) => {
        if (modulePath.indexOf('config') !== -1) {
          delete require.cache[modulePath];
        } else if (modulePath.indexOf('tools') !== -1) {
          delete require.cache[modulePath];
        }
      });
      HotDevelopment = require('./hotDevelopment').default;

      devServer = new HotDevelopment();
    });
  });
});

process.on('SIGTERM', () => devServer && devServer.dispose().then(() => process.exit(0)));
