import config, { clientConfig } from '../config';
import { removeEmpty, ifElse, merge, happyPackPlugin, logJSON } from '../src/util';
import path from 'path';
import appRootDir from 'app-root-dir';
import WebpackMd5Hash from 'webpack-md5-hash';
import webpack from 'webpack';
import AssetsPlugin from 'assets-webpack-plugin';
import nodeExternals from 'webpack-node-externals';

export default function webpackConfigFactory(buildOptions) {
  const  { target, mode } = buildOptions;
  console.log(`Building webpack config for "${target}" in "${mode}" mode`);

  const isDeveleopment = mode === 'development';
  const isProduction = mode === 'production';
  const isClient = target === 'client';
  const isServer = target === 'server';
  const isNode = !isClient;

  const ifDevelopment = ifElse(isDeveleopment);
  const ifProduction = ifElse(isProduction);
  const ifClient = ifElse(isClient);
  const ifDevelopmentClient = ifElse(isDeveleopment && isClient);
  const ifProductionClient = ifElse(isProduction && isClient);
  const ifNode = ifElse(isNode);

  const bundleConfig = isServer || isClient ? config.bundles[target] : config.additionalNodeBundles[target];

  if (!bundleConfig) {
    throw new Error(`No bundle configuration for traget: "${target}"`);
  }

  const webPackConfig = {
    // Set target environment
    // @see: https://webpack.js.org/configuration/target/
    target: isClient ? 'web' : 'node',

    // Customize the NodeJS environment using polyfills or mocks
    // @see: https://webpack.js.org/configuration/node/
    node: {
      __dirname: true,
      __filename: true,
    },

    // Easily exclude node modules in Webpack
    // @see: https://github.com/liady/webpack-node-externals
    externals: removeEmpty([
      ifNode(
        () => nodeExternals(
          {
            whitelist: ['source-map-support'].concat(config.nodeBundlesIncludeNodeModuleFileTypes || [],),
          },
        ),
      ),
    ]),

    // This option controls if and how Source Maps are generated.
    // @see: https://webpack.js.org/configuration/devtool/
    devtool: ifElse(isNode || isDeveleopment || config.includeSourceMapsForProductionBuilds,)(
      'source-map',
      'hidden-source-map',
    ),

    // Control how webpack notifies about assets and entrypoints that exceed a specific file limit
    // @see: https://webpack.js.org/configuration/performance/
    performance:  ifProductionClient({ hints: 'warning' }, false),

    // @see: https://webpack.js.org/configuration/entry-context/
    entry: {
      index: removeEmpty([
        ifNode('source-map-support/register'),
        ifDevelopmentClient('react-hot-loader/patch'),
        ifDevelopmentClient(() => `webpack-hot-middleware/client?reload=true&path=http://${config.host}:${config.clientDevServerPort}/__webpack_hmr`),
        ifClient('regenerator-runtime/runtime'),
        path.resolve(appRootDir.get(), bundleConfig.srcEntryFile),
      ]),
    },

    output: merge({
        path: path.resolve(appRootDir.get(), bundleConfig.outputPath),
        filename: ifProductionClient('[name]-[chunkhash].js', '[name].js'),
        chunkFilename: '[name]-[chunkhash].js',
        libraryTarget: ifNode('commonjs2', 'var'),
      },
      ifElse(isServer || isClient)( () => ({
        publicPath: ifDevelopment(
          `http://${config.host}:${config.clientDevServerPort}${config.bundles.client.webPath}`,
          bundleConfig.webPath,
        ),
      })),
    ),

    resolve: {
      extensions: config.bundleSrcTypes.map(ext => `.${ext}`),
    },

    plugins: removeEmpty([
      ifProduction( () => new CodeSplitPlugin() ),
      ifClient( () => new WebpackMd5Hash() ),

      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(mode),
        'process.env.IS_CLIENT': JSON.stringify(isClient),
        'process.env.IS_SERVER': JSON.stringify(isServer),
        'process.env.IS_NODE': JSON.stringify(isNode),
      }),

      ifClient(() =>
        new AssetsPlugin({
          filename: config.bundleAssetsFileName,
          path: path.resolve(appRootDir.get(), bundleConfig.outputPath),
        }),
      ),

      ifDevelopment(() => new webpack.NoErrorsPlugin()),

      ifDevelopmentClient(() => new webpack.HotModuleReplacementPlugin()),

      ifProductionClient(
        () => new webpack.LoaderOptionsPlugin({
          minimize: config.optimizeProductionBuilds,
        }),
      ),

      ifProductionClient(
        ifElse(config.optimizeProductionBuilds)(
          () => new webpack.optimize.UglifyJsPlugin({
            sourceMap: config.includeSourceMapsForProductionBuilds,
            compress: {
              screw_ie8: true,
              warnings: false,
            },
            mangle: {
              screw_ie8: true,
            },
            output: {
              comments: false,
              screw_ie8: true,
            },
          }),
        ),
      ),

      ifProductionClient(
        () => new ExtractTextPlugin({
          filename: '[name]-[chunkhash].css', allChunks: true,
        }),
      ),

      happyPackPlugin({
        name: 'happypack-javascript',

        loaders: [{
          path: 'babel-loader',
          query: config.plugins.babelConfig(
            // Our "standard" babel config.
            {
              babelrc: false,

              presets: [
                'react',
                'stage-3',
                ifClient(['latest', { es2015: { modules: false } }]),
                ifNode(['env', { targets: { node: true }, modules: false }]),
              ].filter(x => x != null),

              plugins: [
                ifDevelopmentClient('react-hot-loader/babel'),
                ifDevelopment('transform-react-jsx-self'),
                ifDevelopment('transform-react-jsx-source'),
                ifElse(isProduction && (isServer || isClient))(
                  [
                    'code-split-component/babel',
                    {
                      mode: target,
                    },
                  ],
                ),
              ].filter(x => x != null),
            },
            buildOptions,
          ),
        }],
      }),

      ifDevelopmentClient(
        () => happyPackPlugin({
          name: 'happypack-devclient-css',
          loaders: [
            'style-loader',
            {
              path: 'css-loader',
              // Include sourcemaps for dev experience++.
              query: { sourceMap: true },
            },
            { path: 'postcss-loader' },
            {
              path: 'sass-loader',
              options: {
                outputStyle: 'expanded',
                sourceMap: true,
              },
            },
          ],
        }),
      ),

    ]),

    module: {
      rules: removeEmpty([
        // JAVASCRIPT
        {
          test: /\.jsx?$/,
          loader: 'happypack/loader?id=happypack-javascript',
          include: removeEmpty([
            ...bundleConfig.srcPaths.map(srcPath =>
              path.resolve(appRootDir.get(), srcPath),
            ),
            ifProductionClient(path.resolve(appRootDir.get(), 'src/html')),
          ]),
        },

        // CSS
        ifElse(isClient || isServer)(
          merge(
            {
              test: /\.scss|\.css$/,
            },
            ifDevelopmentClient({
              loaders: ['happypack/loader?id=happypack-devclient-css'],
            }),

            ifProductionClient(() => ({
              loader: ExtractTextPlugin.extract({
                fallbackLoader: 'style-loader',
                loader: ['css-loader?sourceMap&importLoaders=2!postcss-loader!sass-loader?outputStyle=expanded&sourceMap&sourceMapContents'],
              }),
            })),
            // When targetting the server we use the "/locals" version of the

            ifNode({
              loaders: ['css-loader/locals', 'postcss-loader', 'sass-loader'],
            }),
          ),
        ),

        // ASSETS (Images/Fonts/etc)
        ifElse(isClient || isServer)(() => ({
          test: new RegExp(`\\.(${config.bundleAssetTypes.join('|')})$`, 'i'),
          loader: 'file-loader',
          query: {
            publicPath: isDeveleopment
              ? `http://${config.host}:${config.clientDevServerPort}${config.bundles.client.webPath}`
              : config.bundles.client.webPath,
            emitFile: isClient,
          },
        })),
      ]),
    },

  };
  // Apply the configuration middleware.
  // logJSON(webPackConfig);
  return config.plugins.webpackConfig(webPackConfig, buildOptions);
}
